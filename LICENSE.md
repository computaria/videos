CC-BY 4.0

[![licneça sobre CC-BY 4.0](https://licensebuttons.net/l/by/3.0/88x31.png)]
(https://creativecommons.org/licenses/by/4.0/)

Veja o [texto completo da licença](https://creativecommons.org/licenses/by/4.0/legalcode)

Creative Commons Corporation (“Creative Commons”) is not a law firm and does
not provide legal services or legal advice. Distribution of Creative Commons
public licenses does not create a lawyer-client or other relationship. Creative
Commons makes its licenses and related information available on an “as-is”
basis. Creative Commons gives no warranties regarding its licenses, any
material licensed under their terms and conditions, or any related information
Creative Commons disclaims all liability for damages resulting from their use
to the fullest extent possible.