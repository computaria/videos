Vídeos que são lançados ganham uma pasta própria. Cada vídeo contém um
script, no mínimo, para auxiliar na sua gravação. Eles vão ser identificados
por ano de publicação, seguido de um sequencial de 3 dígitos e, por fim, o
título do vídeo. Por exemplo, `2018-001-soma_logs`.

Vídeos ainda em produção estão dentro da pasta `000-unreleased`, dentro de uma
pasta com seu título.

Por exemplo, o vídeo `soma_logs`:

```
PRIMEIRO MOMENTO

  +
  |
  +-- 000-unreleased -+-- soma_logs
                      |
                      +-- intersecao_circulos

SEGUNDO MOMENTO

  +
  |
  +-- 000-unreleased -+-- intersecao_circulos
  |
  +-- 2018-001-soma_logs
```

Além do script do vídeo, todo e qualquer material adicional será colocado na
pasta do vídeo.