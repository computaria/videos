Vamos falar sobre computação? Vamos falar de Computaria!

No tópico de hoje, uma propriedade interessante da matemática.
Visto no ensino médio aqui no Brasil e subestimado por muitos,
temos o logaritmo.

Para que precisamos do logaritmo, para que serve ele?

Vamos falar aqui de uma das propriedades do logaritmo:

> O logaritmo do produto é a soma dos logaritmos

> $`\log(A\times B) = \log A + \log B`$

Bem, primeiro vamos demonstrar essa propriedade e depois
vamos entender em como ela pode ajudar na parte da programação?

> ### Pré-requisito
>
> Para acompanhar esse vídeo, é bom saber já o que é:
>
> - noção de operação inversa
> - exponenciação
> - propriedades da exponenciação
> - o que é logaritmo

# Demonstração

> Vai para a fórmula do produto implicando o log **001**

Para começar, temos aqui dois números, $`A`$ e $`B`$, sendo
multiplicados. Eles vão resultar em $`C`$.

Vamos representar $`A`$ em forma de potência de 2: $`A = 2^a`$

> Vai para a demonstração do valor de $`a`$ **002**

Então, conseguimos chegar a conclusão de que $`a`$ existe e
que seu valor é $`\log_2(A)`$. O mesmo se pode fazer para $`B`$
e $`b`$.

> Vai para a fórmula em que iguala $`C = 2^{a + b}`$ e que desenha a
conclusão final **003**

Então, substituindo os valores de $`a`$-zinho e $`b`$-zinho naquela
primeira multiplicação, vemos aqui que $`C = 2^a \times 2^b`$.
Pela definição de exponenciação, isso resulta em somar os índices dos
expoentes $`2^a \times 2^b = 2^{a + b}`$.

Com isso, temos que o logaritmo de $`2^{a + b}`$ é $`a + b`$.
Voltando as origens, obtemos $`2^{a+b}`$ a partir de $`C`$, e $`C`$
obtemos a partir do produto $`A \times B`$. Também sabemos que
$`a = \log_2(A)`$ e que $`b = \log_2(B)`$, portanto chegamos a
conclusão que $`\log_2(A \times B) = \log_2(A) + \log_2(B)`$.
Em outras palavras, o logaritmo do produto é igual à soma dos logaritmos.

Mais detalhes dessa demonstração, conforme apresentada aqui, no [link
na descrição](./demonstracao.md).

# Exemplo de uso

Trabalhando na GeoSales, deparei-me com a seguinte situação:
Preciso calcular o preço de venda de um produto a partir
de múltiplos descontos, descontos esses fornecidos como
um porcentual.

No final das contas, precisávams fazer um produtório desse jeito:

> fórmula do produtório **004**

> Parênteses: Para quem não está acostumado, produtório é como se
fosse um somatório, só que em vez de adicionar (obter soma),
vamos multiplicar (obter produto)

O que isso quer dizer? Quer dizer que, caso seja dado um desconto
de 10%, é o mesmo que pegar o preço base do produto e multiplicar
por $`(1 - 10\%) = 0.9`$, e que aplicar desconto sobre desconto
é fazer várias dessas multiplicações

Programando, a nível de Java, é tranquilo fazer esse produtório.
Mas o SQL não possui essa operação! Ele possui apenas somatório!
E o que acabamos de ver que transforma produto em soma? Exatamente,
logaritmo!

> Mostrar imagem da transformação de produtório para somatório **005**

Então, para transformar produtório em somatório, o que eu fiz foi:
elevar a base natural $`e`$ o somatório dos logaritmos.

Veja a transformação, passo a passo. Comecemos por aplicar
o exponencial e sua operação inversa, o logaritmo. Daí
obtemos que o produto entre dois números quaisquer é
o exponencial do somatório de seus logaritmos.

Podemos extrapolar isso para um conjunto de números, então
transformamos o produtório em um exponencial de: somatório de: logaritmos de:
elementos.

Com isso pude transformar numa consulta significantemente parecida com essa:

> Mostrar consulta em SQL **006**

Em que fazemos justamente o **exponencial de: somatório de: logaritmos de:
elementos**. Note que, para o caso de desconto do item, o elemento é
$`1 - desconto_i`$, qualquer que seja o desconto.

Para ver essa consulta funcionando com uma base de testes, veja o [link na
descrição](http://sqlfiddle.com/#!6/600220/1). Fica ligado também que o
conteúdo desse vídeo todinho está no nosso repositório no [gitlab.com]
(./), assim como possivelmente algum material adicional.

Com isso, terminamos o vídeo de hoje. Falous!, e fiquem ligados para mais
Computaria!