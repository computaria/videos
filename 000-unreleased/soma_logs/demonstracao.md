```math
A\times B = C
```
```math
\therefore
```
```math
\log_2(A\times B) = \log_2(C)
```

Podemos dizer que $`A = 2^a`$, para algum $`a`$ que satisfaça
essa propriedade. O valo de $`a`$ deve ser precisamente $`a = \log_2(A)`$,
já que o $`\log `$ é o operador inverso da potenciação. Como demonstrar isso?

```math
A = 2^a \therefore \log_2(A) = \log_2(2^a) = a
```
```math
a = \log_2(A)
```

Pode-se falar a mesma coisa analogamente a $`B`$ e $`b`$. Então:

```math
A\times B = 2^a \times 2^b = 2^{a + b} = C
```
```math
\log_2(A\times B) = \log_2(C) = \log_2(2^{a + b}) = a + b
```
```math
\log_2(A\times B) = a + b = \log_2(A) + \log_2(B)
```

O mesmo pode ser encontrado para qualquer outra base.

Válido para $`A,B \in \mathbb{R}^+_*`$