Trabalhando na GeoSales, deparei-me com a seguinte situação:
Preciso calcular o preço de venda de um produto a partir
de múltiplos descontos, descontos esses fornecidos como
um porcentual.

Uma estratégia ingênua seria calcular o valor absoluto de cada
desconto e subtrair do total. Por exemplo, se o vendedor fornecer
os descontos de 20% e 20% num item que custa 100, essa estratégia
retornaria 40 de desconto absluto, portanto 60 de preço de venda.

Em termos de fórmular matemática, seria assim:

```math
vr\_final = vr\_inicial - \sum_i vr\_inicial \times desconto_i
```

Só que isso pode gerar alguns absurdos, como por exemplo um desconto
de 50% e outro de 60% implicar em vender o produto a -10:

```math
vr\_final = 100 - \sum_i 100 \times desconto_i
```
```math
vr\_final = 100 - (100 \times 50\% + 100 \times 60\%) = 100 - 110 = -10
```
```math
vr\_final = -10
```

Obviamente a matemática aqui não está batendo bem.

A questão aqui é que o desconto aplicado é sobre o desconto anterior.
Então, dar desconto de 10% equivale a remover 10% do valor inicial
$`vr\_desconto = vr\_inicial \times (1 - desconto)`$.
Como aplicar desconto é uma operação multiplicativa, aplicar múltiplos
desontos é só fazer mais uma multiplicação:

```math
vr\_desconto = vr\_inicial \times (1 - desconto_1) \times (1 - desconto_2)
```

Se adicionar um desconto a mais, basta multiplicar por $`(1 - desconto_i)`$.
Assim como existe o somatório para fazer a soma de múltiplos elementos, temos
o produtório para fazer o produto de vários elementos, indicado por $`\prod`$:

```math
vr\_final = vr\_inicial \times \prod_i (1 - desconto_i)
```

Até aí, tudo bem, tudo tranquilo. Como a operação de multiplicação nos
números reais é comutativa, não é necessário especificar a ordem com que
é feito esse produto.

Conseguimos manusear tudo a nível da
nossa aplicação. Mas, então, precisamos verificar no banco de dados
o valor do item. Como faremos isso?

Usando produtório! É claro! Mas... no SQL não tem isso... No SQL temos somatório,
mas não produtório. Como proceder?

Bem, com matemática! Nós sabemos que o logaritmo do produto de dois números
é a soma dos logaritmos desses números. Também sabemos que a operação
inversa do logaritmo é a exponenciação. Então:

```math
A \times B = e^{\log(A \times B)} \therefore
```
```math
A \times B = e^{\log A + \log B} \therefore
```
```math
\prod_i El_i = e^{\sum_i \log El_i}
```

Com isso, conseguimos transformar um produtório em um
somatório. No banco de dados, seria feita a seguinte consulta:

```sql
SELECT
	i.chave,
	i.vr_inicial * exp(sum(log(1 - d.desconto))) as valor_venda
FROM
	item i
		INNER JOIN item_desconto d ON i.chave = d.item_chave
GROUP BY
	i.vr_inicial,
	i.chave 
```

Veja funcionando no [SQL Fiddle](http://sqlfiddle.com/#!6/600220/1).