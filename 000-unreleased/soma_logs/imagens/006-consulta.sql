SELECT
    i.chave,
    i.vr_inicial * exp(sum(log(1 - d.desconto))) as valor_venda
FROM
    item i
        INNER JOIN item_desconto d ON i.chave = d.item_chave
GROUP BY
    i.vr_inicial,
    i.chave 
