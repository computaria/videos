Existem alguns _caveats_ ao
se usar o `tc-compiler-help`, que até onde eu me lembro tem muito derivado do
próprio TotalCross (pelo menos na versão 4.X.Y, que foi de quando o projeto surgiu)
e **uma** condição que realmente foi imposição minha para trabalhar de modo mais
controlado com o Maven: fazer a construção do app final sobre o arquivo `.jar`.

Assim sendo, o nome do arquivo precisa necessariamente ser o nome da `MainWindow`.
Por isso que existe o método `Closeable createCopiedJar(Class<? extends MainWindow> mainWindowClass)`,
para esconder isso do programador.

Outra limitação é em relação à classe que chama o `CompilationBuilder`. De modo geral,
ela se encontra dentro do mesmo artefato Maven que será gerado. Isso significa que
ela precisa ser capaz de sofrer um `tc.Deploy`.

Algumas limitações entram aí. A primeira é que você não pode fazer chamadas a APIs Java
que o TotalCross não conheça. Na versão que estou usando, 4.3.8, TotalCross não sabe lidar
com `java.io.File`, por exemplo.

Existem 2 contornos possíveis para isso. O primeiro é deixar a configuração do `CompilationBuilder`
em outro projeto dentro do mesmo reator Maven, mas eu creio que essa não seja uma solução
adequada. A segunda, que eu particularmente acho mais interessante, é deixar a
configuração do `CompilationBuilder` porém com dependências de classes que _não sofrerão `tc.Deploy`_.
Essa alternativa implica que essas dependências estejam dentro do mesmo reator, porém geradas
em outro artefato.