Vídeo resposta ao [vídeo da TotalCross](https://youtu.be/Cq5yEPTmZWI) em que se diz que se coloca uma biblioteca externa própria

Aqui, mostramos a alternativa de API programática fluente
[`tc-compiler-help`](https://gitlab.com/geosales-open-source/tc-compiler-help) para gerenciar suas
próprias bibliotecas.

Link do vídeo: https://youtu.be/NHPI4oh47h0