Este vídeo é uma alternativa de como se gerar
uma biblioteca externa para um aplicativo TotalCross.
Este vídeo é uma alternativa ao que o Bruno, da própria TotalCross,
apresentou recentemente nessa temática de dependências externas.

Vamos lá ao passo-a-passo proposto pelo Bruno:

1. criar a biblioteca externa
2. adicionar o plugin do TC no POM da dependência
3. não colocar a tag `<platforms>` nas configurações do plugin
4. adicionar `Lib` no final da tag `<name>` nas configurações do plugin
5. rodar `TotalCross Package` pelo VSCode
    - alternativamente chamar o `tc.Deploy` por qualquer outra IDE
6. criar o arquivo `all.pkg` dentro do projeto do seu aplicativo
7. colocar `[L] /absolute/path/myLib.tcz` no `all.pkg`

Isso até pode ficar viável para uma, duas, dez dependências. Mas, e se eu
escalar para algo em torno de 20?

Onde eu trabalho, na GeoSales, são 24 dependências, dentre elas:

- 4 bibliotecas externas ao projeto de código aberto
- 3 bibliotecas externas ao projeto de código fechado
- 17 bibliotecas internas ao projeto (alcançáveis pelo mesmo reator Maven)

Como fazer para manter a coerência com os 7 passos descritos acima?
A resposta é: automatizar.

Essa automatização foi feita enquanto se tinham 4 dependências:

- 3 bibliotecas externas ao projeto de código fechado
- 1 biblioteca externa ao projeto de código aberto

E projetamos a escala para que não fosse necessário alterar a compilação
caso mantívessemos dependências que respeitassem um padrão (que, no caso,
era o nome da empresa como sendo parte do `groupId`). Desde então, não foi
necessária nenhuma manutenção para atualizar as dependências.

Como isso é possível? Basicamente, usando o [`tc-compiler-help`](https://gitlab.com/geosales-open-source/tc-compiler-help).
Nele, há uma API fluente para que você possa definir diversos parâmetros de compilação.
Ele irá inspecionar todo o _classpath_ em tempo de execução para saber
quais são as dependências que você gostaria que fossem embarcadas juntas
no seu projeto.

Vamos pegar um projeto de exemplo aqui, o [`stream-support-totalcross-sample`](https://gitlab.com/geosales-open-source/stream-support-totalcross-sample).
Esse exemplo foi criado só para demonstrar a possibilidade de usar algo
extremamente semelhante ao `java.util.stream.Stream` do Java 8 dentro do TotalCross,
através do projeto [`totalcross-functional-toolbox`](https://gitlab.com/geosales-open-source/totalcross-functional-toolbox).

> Exibir o Eclipse com o projeto

O projeto em si é bem simples, contém 3 classes apenas:

- a classe `App` que estende a `MainWindow`
- a classe `SampleApp` que apenas chama `TotalCrossApplication.run` para a classe principal
- a classe `CompileApp` que configura o `CompilationBuilder` e lida com parâmetros CLI

Vamos focar na configuração principal do `CompilationBuilder`. As primeiras coisas que se podem
ver são que estamos configurando variáveis relativas ao TotalCross:

- a chave usada do TotalCross
- onde está TotalCross

Em seguida, começamos a ver coisas relativas à construção propriamente dita, quando
se informa que se deseja compilar para WIN32. O próximo passo é informando o _predicado_
de compilação da dependência. Esse projeto em específico foi feito para mostrar usando
`Stream` no TotalCross, então foi necessário colocar lá algo que conseguisse colocar identificar
corretamente a dependência do `totalcross-functional-toolbox`.

Depois, finalmente, temos a classe da `MainWindow`, informação de que se trata de um
"_single package_" e o comando para se fazer o `.build()`.

Chamando pela linha de comando pelo Maven, seria algo assim:

```bash
./mvnw clean package exec:java -Dexec.mainClass="br.com.softsite.streamsupport.CompileApp" \
    -Dexec.args="--tc-key 5443444B43554F0ADBF5E97E --tc-home ../computaria/tc-4.3.8/TotalCross" \
    -P retrolambda
```

> Essa chave apresentada se encontra [num repositório da TotalCross](https://github.com/TotalCross/HomeApplianceXML/blob/master/pom.xml#L10)

No fim das contas, o `tc-compiler-help` serve para 3 propósitos:

1. preparar a linha de comando para se chamar o `tc.Deploy`
2. compilar as dependências para `.tcz` sob demanda
3. manter dinamicamente o arquivo `all.pkg`

De modo geral, era isso que eu gostaria de falar. O Bruno falou, em seu vídeo original, os requisitos necessários
para se colocar uma biblioteca externa dentro de uma aplicação TotalCross. Nominalmente,
você precisa gerar um `.tcz` da biblioteca (que precisa terminar como `Lib.tcz`) e, então,
mencioná-la no `all.pkg`. Além desse
passo necessário, ele também deu a sugestão de fazer a transformação do `.jar` em `.tcz`
através da chamada o plugin da TotalCross.

Aqui neste vídeo, coloquei como contraponto um processo de automatização do processo de
gerar o `all.pkg` e gerar, sob demanda, os `.tcz`s das bibliotecas, bastando apenas
informar com base em uma condição se uma biblioteca precisa ou não ser transformada em
`.tcz`. O próprio `tc-compiler-help` se preocupa em observar o _classpath_ gerado
pelo Maven para identificar quais os `.jar`s que precisam ser transformados.

Até mais e valeu!